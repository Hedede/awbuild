targets = test

# shared|static|executable
targets.test.type = shared
targets.test.cxx.flags = debug
targets.test.sources = test.cxx

all: $(targets)
include rules.mk
