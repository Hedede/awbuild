include toolchains/linux-gcc.mk

define create-target
$(eval temp.sources=$(targets.$1.sources))
$(eval temp.bdir   = build/$1)
$(eval temp.objects=$(patsubst %.cxx,$(temp.bdir)/%.o, $(temp.sources)))

$(temp.objects) : $(temp.bdir)/%.o: %.cxx
	mkdir -p $$(dir $$@)
	$(cxx.compiler) $(cxx.flags) -c $$< -o $$@

$1: $(temp.objects)
	$(cxx.compiler) $$^ -o $$@
endef
$(foreach target,$(targets),$(eval $(call create-target,$(target))))
